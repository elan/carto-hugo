# Inclusion des infos (description, permalink, etc.) dans l'encodage des surfaces

## Etape 1 : xslx vers csv
Enregistrer avec libreOffice ou autre en csv avec "#" comme séparateur de colonne (tjs plus sûr qu'une ponctuation qui peut se retrouver dans la description ou autre)

## Etape 2 : ligne de commande pour en faire un XML manipulable
cat cartohugo_donnees-15-05-2024.csv | sed '1d' | awk -F'#' 'BEGIN { print "<update>"}{ print "<surf><id>"$1"</id><label>"$3"</label><desc>"$4"</desc><link type=\"permalien\" target=\""$2"\"/></surf>";} END { print "</update>"}' | sed 's#&#&amp;#g' >cartohugo_donnees-15-05-2024_update.xml ;

## Etape 3 : transfo XSL
saxonb-xslt ../tei/cartohugo_travailleursdelamer.xml ./updateSurfaces_20240529.xsl >cartohugo_travailleursdelamer_patch20240529.xml

## Etape 4 : remplacement du fichier dans ../tei/
cp cartohugo_travailleursdelamer_patch20240529.xml ../tei/cartohugo_travailleursdelamer.xml
