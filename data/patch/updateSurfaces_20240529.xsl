<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating imgToDesc.json
* version 2024-05-29
* @author AnneGF@CNRS
* @date : 2022-2024
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:variable name="update" select="document('cartohugo_donnees-15-05-2024_update.xml')"/>
    
    <!-- Match nodes, processing-instruction and comments -->
    <xsl:template match="* | comment() | processing-instruction()">
        <!-- Copy the element -->
        <xsl:copy>
            <!-- Copy its attributes -->
            <xsl:for-each select="attribute::*">
                <xsl:attribute name="{name(.)}" select="."/>
            </xsl:for-each>
            <!-- Go on in the XML tree -->
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="tei:surface">
        <xsl:variable name="id" select="@xml:id"/>
        <xsl:variable name="to_add" select="$update//surf[child::id = $id]"/>
        <surface xml:id="{@xml:id}" xmlns="http://www.tei-c.org/ns/1.0">
            <label><xsl:value-of select="$to_add/label"/></label>
            <desc><xsl:value-of select="$to_add/desc"/></desc>
            <link type="permalien" target="{$to_add/link/@target}"/>
            <xsl:apply-templates/>
        </surface>
    </xsl:template>
    
    
</xsl:stylesheet>