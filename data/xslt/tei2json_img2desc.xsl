<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating imgToDesc.json
* version 2024-05-29
* @author AnneGF@CNRS
* @date : 2022-2024
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
    
    <!--
        This XSL use a XML-TEI input file and generate a json file containing a list of locations (with zone information) organized by image based on <tei:surface/>)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
        Output example:
        ```
        {
            "CG_1781.jpg": {
                "Ancresse": [
                    ["2627", "4323", "108", "47"]
                ],
                (...)
            },
            (...)
        }
    -->
    <xsl:variable name="DEBUG">0</xsl:variable>
    
    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>
    
    <xsl:template match="/tei:TEI">
        <xsl:text>{</xsl:text>
        <xsl:value-of select="$br"/>
        <xsl:for-each select="tei:sourceDoc/tei:surface">
            <xsl:text>  "</xsl:text>
            <xsl:value-of select="@xml:id"/>
            <xsl:text>": {</xsl:text>
            <xsl:value-of select="$br"/>
            <!-- source -->
            <xsl:text>    "source" : "</xsl:text>
            <xsl:value-of select="tei:link/@target"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$br"/>
            <!-- title -->
            <xsl:text>    "title" : "</xsl:text>
            <xsl:value-of select="tei:label"/>
            <xsl:text>",</xsl:text>
            <xsl:value-of select="$br"/>
            <!-- desc -->
            <xsl:text>    "desc" : "</xsl:text>
            <xsl:variable name="s"><xsl:text>"</xsl:text></xsl:variable>
            <xsl:variable name="r"><xsl:text>\\"</xsl:text></xsl:variable>
            <xsl:value-of select="replace(normalize-space(tei:desc),$s,$r)"/>
            <xsl:text>"</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>  }</xsl:text>
            <xsl:if test="not(position() = last())">
                <xsl:text>,</xsl:text>
            </xsl:if>
            <xsl:value-of select="$br"/>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
        <xsl:value-of select="$br"/>
    </xsl:template>
</xsl:stylesheet>
