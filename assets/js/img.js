window.onbeforeunload = function(){
    messageToParent({'close': imageName})
    self.close();
};

function messageToParent(message) {
  window.opener.ProcessChildMessage(message);
}

async function getData() {
  const response = await fetch("data/auto/js/imgToLocations.json");
  return response.json();
}

let imgUrl = null
let imageName = null
let zoom = false

function processParentMessage(message) {
  imgUrl = message.url
  imageName = imgUrl.substring(imgUrl.lastIndexOf('/') + 1)
  let currentLocation = message.currentLocation
  let viewer = OpenSeadragon({
    id: "seadragon-viewer",
    showNavigator: true,
    showRotationControl: true,
    prefixUrl: '',
    zoomInButton: 'osd-zoom-in',
    zoomOutButton: 'osd-zoom-out',
    homeButton: 'osd-home',
    fullPageButton: 'osd-full-page',
    nextButton: 'osd-next',
    previousButton: 'osd-previous',
    rotateLeftButton: 'osd-left',
    rotateRightButton: 'osd-right',
  });

  viewer.innerTracker.keyHandler = null

  viewer.addHandler('open', function() {
    let viewport = viewer.viewport
    let currentImage = imageName
    let overlay = viewer.svgOverlay();

    getData().then((data) => {
      let jsonMention = data
      zoom = false
      let i = 0
      if (jsonMention.hasOwnProperty(currentImage)) {
        for (var location in jsonMention[currentImage]) {
          i++
          if (Object.prototype.hasOwnProperty.call(jsonMention[currentImage], location)) {
            jsonMention[currentImage][location].forEach(function(mention) {
              viewer = makeOverlay(i, mention, viewport, currentLocation, location, viewer)
            })
            if (zoom) {
              viewport.fitBounds(realCoordToZoom, true)
            }
          }
        }
      }

      // $(window).resize(function() {
      //   overlay.resize();
      // });
    })

  });

  viewer.open({
    type: 'image',
    url: imgUrl
  });
}

const makeOverlay = function(i, mention, viewport, currentLocation, location, viewer) {
  let x = parseInt(mention[0], 10)
  let y = parseInt(mention[1], 10)
  let width = parseInt(mention[2], 10)
  let height = parseInt(mention[3], 10)
  let openseaCoord = new OpenSeadragon.Rect(x, y, width, height)
  let realCoord = viewport.imageToViewportRectangle(openseaCoord)
  let locationMatch = (currentLocation == location) ? true : false
  let elt = document.createElement("g")
  elt.className = (locationMatch) ? "overlay-active" : "overlay"
  elt.title = location
  elt.id = "overlay" + i
  elt.style.zIndex = 9999999;

  viewer.addOverlay({
    element: elt,
    location: realCoord
  })

  let tracker = new OpenSeadragon.MouseTracker({
    element: elt.id,
    clickHandler: function(event) {
      messageToParent({'choose-location': location})
    }
  })

  if (locationMatch && !zoom) {
    realCoordToZoom = realCoord
    zoom = true
  }

  return viewer
}
