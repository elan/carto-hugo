function handleTabClick() {
    /*
        - appelé au click sur un tab
        - récupère la target en fonction du tab cliqué
        - appelle toggleTab()
    */

    // get target du tab cliqué
    var data_target = this.dataset.target;
    // get target element
    var target = document.querySelector('.tab_target[data-target="' + data_target + '"');

    toggleTab(this, target);
}

function toggleTab(tab, target) {
    /*
        - permet de montrer la cible correspondant a un tab
        - ajoute et reset .active pour .tab_bar_item et .tab_target
    */

    // reset des class .active
    var tab_already_active = document.querySelector(".tab_item.active");
    if (tab_already_active) tab_already_active.classList.remove("active");
    // ajout .active sur le tab voulu
    tab.classList.add("active");

    var target_already_active = document.querySelector(".tab_target.active");
    if (target_already_active) target_already_active.classList.remove("active");
    // reset des class .active autre que la div voulue
    target.classList.add("active");
    // ajout .active sur la div voulue
}