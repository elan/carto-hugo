
document.addEventListener("DOMContentLoaded", function() {
    init();
}, false);

async function init() {

    var data = await getData("../../assets/json/stats.json");
    // console.log(data);

    var ul = document.createElement("UL");

    var li_preliminary = document.createElement("LI");
    var span_preliminary = document.createElement("SPAN");
    span_preliminary.innerHTML = data.preliminary.name;
    li_preliminary.appendChild(span_preliminary);

    var max_occ = getMaxOcc(data);

    var chaps_ul = document.createElement("UL");
    for (var chap in data.preliminary.chaps) {
        var li_chap = document.createElement("LI");
        var span_chap = document.createElement("SPAN");
        span_chap.innerHTML = "chap. " + data.preliminary.chaps[chap].name;
        li_chap.appendChild(span_chap);

        chaps_ul.appendChild(li_chap);
        
        var places_ul = document.createElement("UL");
        places_ul.classList.add("gallery");
        for (var place in data.preliminary.chaps[chap].places) {

            var clone = document.querySelector(".place_template").content.cloneNode(true);
            clone.querySelector(".place_name").innerHTML = place.replaceAll('#', '');
            clone.querySelector(".place_img").src = "assets/svg/" + place.replaceAll('#', '') + ".svg";

            var occ = data.preliminary.chaps[chap].places[place];
            var num = scale(occ, 1, max_occ, 1, 100);
            clone.querySelector(".place_diagram").setAttribute("style", "--value: "+num+";");
            clone.querySelector(".place_data").innerHTML = occ + " occ.";

            places_ul.appendChild(clone);
        }

        chaps_ul.appendChild(places_ul);
        

    }
    
    li_preliminary.appendChild(chaps_ul);
    ul.appendChild(li_preliminary);



    var li_oeuvre = document.createElement("LI");
    var span_oeuvre = document.createElement("SPAN");
    span_oeuvre.innerHTML = data.oeuvre.name;;
    li_oeuvre.appendChild(span_oeuvre);

    var parts_ul = document.createElement("UL");
    for (var part in data.oeuvre.parts) {
        var li_part = document.createElement("LI");
        var span_part = document.createElement("SPAN");
        span_part.innerHTML = data.oeuvre.parts[part].name;
        li_part.appendChild(span_part);

        parts_ul.appendChild(li_part);

        var livres_ul = document.createElement("UL");
        for (var livre in data.oeuvre.parts[part].livres) {
            var li_livre = document.createElement("LI");
            var span_livre = document.createElement("SPAN");
            span_livre.innerHTML = data.oeuvre.parts[part].livres[livre].name;
            li_livre.appendChild(span_livre);

            livres_ul.appendChild(li_livre);

            var chaps_ul = document.createElement("UL");
            for (var chap in data.oeuvre.parts[part].livres[livre].chaps) {
                var li_chap = document.createElement("LI");
                var span_chap = document.createElement("SPAN");
                span_chap.innerHTML = "chap. " + data.oeuvre.parts[part].livres[livre].chaps[chap].name;
                li_chap.appendChild(span_chap);

                chaps_ul.appendChild(li_chap);

                var places_ul = document.createElement("UL");
                places_ul.classList.add("gallery");
                for (var place in data.oeuvre.parts[part].livres[livre].chaps[chap].places) {
                
                    var clone = document.querySelector(".place_template").content.cloneNode(true);
                    clone.querySelector(".place_name").innerHTML = place.replaceAll('#', '');
                    clone.querySelector(".place_img").src = "assets/svg/" + place.replaceAll('#', '') + ".svg";

                    var occ = data.oeuvre.parts[part].livres[livre].chaps[chap].places[place];
                    var num = scale(occ, 1, max_occ, 1, 100);

                    clone.querySelector(".place_diagram").setAttribute("style", "--value: "+num+";");
                    clone.querySelector(".place_data").innerHTML = occ + " occ.";

                    places_ul.appendChild(clone);
                }

                li_chap.appendChild(places_ul);

            }
    
            li_livre.appendChild(chaps_ul);

        }

        li_part.appendChild(livres_ul);

    }

    li_oeuvre.appendChild(parts_ul);
    ul.appendChild(li_oeuvre);

    var visualisation = document.querySelector("#visualisation");
    visualisation.appendChild(ul);

        calculMax(data);

}

async function getData(path) {
    var options = {
        method: 'GET',
        mode: "cors"
    };
    var response = await fetch(path, options);
    var text = await response.text();
    var json = JSON.parse(text);
    return json;
}

function getMaxOcc(data) {
    var values = Object.values(data.chap_max_place_number);
    var value_max = Math.max(...values);
    return value_max;
}

function scale(number, inMin, inMax, outMin, outMax) {
    /*
        interval de départ : outMin, outMax
        interval à atteindre : outMin, outMax
    */
    return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}