
var zones = {
    "europe": [
        {
            "ulx": "6762", "uly": "2004",
            "lrx": "6904", "lry": "2064",
            "corresp": "#Aiguilles_Wight"
        },
        { 
            "ulx": "8228", "uly": "1927",
            "lrx": "179", "lry": "46",
            "corresp": "#Ambleteuse_Baie"
        },
        { 
            "ulx": "6682", "uly": "6616",
            "lrx": "6732", "lry": "6670",
            "corresp": "#Astigarraga"
        },
        { 
            "ulx": "6566", "uly": "2562",
            "lrx": "6738", "lry": "2660",
            "corresp": "#Aurigny"
        },
        { 
            "ulx": "6154", "uly": "6624",
            "lrx": "6272", "lry": "6708",
            "corresp": "#Bilbao"
        },
        { 
            "ulx": "6310", "uly": "3402",
            "lrx": "6390", "lry": "3446",
            "corresp": "#Binic"
        },
        { 
            "ulx": "6304", "uly": "3226",
            "lrx": "6448", "lry": "3272",
            "corresp": "#Brehat"
        },
        { 
            "ulx": "5626", "uly": "3452",
            "lrx": "5718", "lry": "3536",
            "corresp": "#Brest"
        },
        { 
            "ulx": "6712", "uly": "3338",
            "lrx": "6780", "lry": "3366",
            "corresp": "#Cancale"
        },
        { 
            "ulx": "6770", "uly": "2812",
            "lrx": "6862", "lry": "2852",
            "corresp": "#Carteret"
        },
        { 
            "ulx": "6324", "uly": "2584",
            "lrx": "6508", "lry": "2646",
            "corresp": "#Casquets"
        },
        { 
            "ulx": "6856", "uly": "2712",
            "lrx": "6954", "lry": "2864",
            "corresp": "#Cherbourg"
        },
        { 
            "ulx": "6640", "uly": "3178",
            "lrx": "6780", "lry": "3224",
            "corresp": "#Chousey"
        },
        { 
            "ulx": "6938", "uly": "3072",
            "lrx": "7126", "lry": "3126",
            "corresp": "#Coutances"
        },
        { 
            "ulx": "7982", "uly": "2522",
            "lrx": "8084", "lry": "2602",
            "corresp": "#Dieppe"
        },
        { 
            "ulx": "6672", "uly": "3484",
            "lrx": "6772", "lry": "3544",
            "corresp": "#Dinan"
        },
        { 
            "ulx": "6274", "uly": "2988",
            "lrx": "6438", "lry": "3044",
            "corresp": "#Douvres"
        },
        { 
            "ulx": "8578", "uly": "1758",
            "lrx": "8754", "lry": "1818",
            "corresp": "#Dunkerque"
        },
        { 
            "ulx": "7668", "uly": "2674",
            "lrx": "7770", "lry": "2706",
            "corresp": "#Etretat"
        },
        { 
            "ulx": "5842", "uly": "1930",
            "lrx": "5998", "lry": "1982",
            "corresp": "#Exeter"
        },
        { 
            "ulx": "6876", "uly": "3248",
            "lrx": "7036", "lry": "3292",
            "corresp": "#Granville"
        },
        { 
            "ulx": "7516", "uly": "1454",
            "lrx": "7638", "lry": "1528",
            "corresp": "#Greenwich"
        },
        { 
            "ulx": "6056", "uly": "3970",
            "lrx": "6206", "lry": "4026",
            "corresp": "#Groix"
        },
        { 
            "ulx": "6256", "uly": "2730",
            "lrx": "6428", "lry": "2840",
            "corresp": "#Guernesey"
        },
        { 
            "ulx": "9432", "uly": "832",
            "lrx": "9556", "lry": "878",
            "corresp": "#Harlem"
        },
        { 
            "ulx": "6428", "uly": "2758",
            "lrx": "6492", "lry": "2784",
            "corresp": "#Herm"
        },
        { 
            "ulx": "6664", "uly": "6628",
            "lrx": "6702", "lry": "6656",
            "corresp": "#Hernani"
        },
        { 
            "ulx": "7110", "uly": "2934",
            "lrx": "7214", "lry": "2978",
            "corresp": "#Isigny"
        },
        { 
            "ulx": "6518", "uly": "2942",
            "lrx": "6644", "lry": "3028",
            "corresp": "#Jersey"
        },
        { 
            "ulx": "7036", "uly": "4914",
            "lrx": "7248", "lry": "4960",
            "corresp": "#La_Rochelle"
        },
        { 
            "ulx": "7374", "uly": "1364",
            "lrx": "7556", "lry": "1464",
            "corresp": "#Londres"
        },
        { 
            "ulx": "5996", "uly": "3868",
            "lrx": "6136", "lry": "3938",
            "corresp": "#Lorient"
        },
        { 
            "ulx": "6566", "uly": "3086",
            "lrx": "6716", "lry": "3122",
            "corresp": "#Minquiers"
        },
        { 
            "ulx": "6878", "uly": "4194",
            "lrx": "7032", "lry": "4274",
            "corresp": "#Nantes"
        },
        { 
            "ulx": "8798", "uly": "1620",
            "lrx": "8918", "lry": "1664",
            "corresp": "#Ostende"
        },
        { 
            "ulx": "8500", "uly": "3168",
            "lrx": "8692", "lry": "3254",
            "corresp": "#Paris"
        },
        { 
            "ulx": "6674", "uly": "6656",
            "lrx": "6732", "lry": "6764",
            "corresp": "#Pasages"
        },
        { 
            "ulx": "6782", "uly": "2850",
            "lrx": "6878", "lry": "2884",
            "corresp": "#Portbail"
        },
        { 
            "ulx": "6326", "uly": "2054",
            "lrx": "6466", "lry": "2120",
            "corresp": "#Portland"
        },
        { 
            "ulx": "6264", "uly": "3374",
            "lrx": "6346", "lry": "3430",
            "corresp": "#Portrieux"
        },
        { 
            "ulx": "7046", "uly": "1738",
            "lrx": "7226", "lry": "1920",
            "corresp": "#Portsmouth"
        },
        { 
            "ulx": "6070", "uly": "3848",
            "lrx": "6202", "lry": "3886",
            "corresp": "#Quimperle"
        },
        { 
            "ulx": "6748", "uly": "3764",
            "lrx": "6940", "lry": "3814",
            "corresp": "#Rennes"
        },
        { 
            "ulx": "7102", "uly": "5028",
            "lrx": "7316", "lry": "5092",
            "corresp": "#Rochefort"
        },
        { 
            "ulx": "8006", "uly": "2786",
            "lrx": "8156", "lry": "2868",
            "corresp": "#Rouen"
        },
        { 
            "ulx": "6290", "uly": "3442",
            "lrx": "6430", "lry": "3498",
            "corresp": "#Saint-Brieuc"
        },
        { 
            "ulx": "6452", "uly": "2790",
            "lrx": "6580", "lry": "2860",
            "corresp": "#Serk"
        },
        { 
            "ulx": "6876", "uly": "1766",
            "lrx": "7102", "lry": "1840",
            "corresp": "#Southampton"
        },
        { 
            "ulx": "7558", "uly": "1404",
            "lrx": "7752", "lry": "1482",
            "corresp": "#Tamise"
        },
        { 
            "ulx": "5942", "uly": "2126",
            "lrx": "6042", "lry": "2172",
            "corresp": "#Tor Bay"
        },
        { 
            "ulx": "8108", "uly": "2458",
            "lrx": "8204", "lry": "2538",
            "corresp": "#Treport"
        },
        { 
            "ulx": "5768", "uly": "1012",
            "lrx": "6250", "lry": "1112",
            "corresp": "#Wales"
        },
        { 
            "ulx": "6894", "uly": "1958",
            "lrx": "7062", "lry": "2040",
            "corresp": "#Wight"
        },
        { 
            "ulx": "9690", "uly": "790",
            "lrx": "9946", "lry": "912",
            "corresp": "#Zuydersee"
        }
    ],
    "archipel": [
        {
            "ulx": "2627", "uly": "4323",
            "lrx": "2735", "lry": "4370",
            "corresp": "#Ancresse"
        },
        {
            "ulx": "3808", "uly": "3188",
            "lrx": "4333", "lry": "3473",
            "corresp": "#Aurigny"
        },
        {
            "ulx": "2628", "uly": "4773",
            "lrx": "2733", "lry": "4821",
            "corresp": "#B_08"
        },
        {
            "ulx": "5578", "uly": "5136",
            "lrx": "5818", "lry": "5218",
            "corresp": "#Barneville"
        },
        {
            "ulx": "1666", "uly": "9432",
            "lrx": "1802", "lry": "9504",
            "corresp": "#Binic"
        },
        {
            "ulx": "2549", "uly": "4391",
            "lrx": "2635", "lry": "4460",
            "corresp": "#Braye_du_Valle"
        },
        {
            "ulx": "2865", "uly": "4791",
            "lrx": "3139", "lry": "4864",
            "corresp": "#Brecqhou"
        },
        {
            "ulx": "4162", "uly": "7814",
            "lrx": "4316", "lry": "7870",
            "corresp": "#Canard"
        },
        {
            "ulx": "4916", "uly": "9156",
            "lrx": "5084", "lry": "9210",
            "corresp": "#Cancale"
        },
        {
            "ulx": "5378", "uly": "5126",
            "lrx": "5536", "lry": "5242",
            "corresp": "#Carteret"
        },
        {
            "ulx": "2963", "uly": "3058",
            "lrx": "3245", "lry": "3145",
            "corresp": "#Casquets"
        },
        {
            "ulx": "2379", "uly": "4544",
            "lrx": "2487", "lry": "4579",
            "corresp": "#Catel"
        },
        {
            "ulx": "5916", "uly": "3734",
            "lrx": "6212", "lry": "3806",
            "corresp": "#Cherbourg"
        },
        {
            "ulx": "2195", "uly": "4783",
            "lrx": "2278", "lry": "4840",
            "corresp": "#Corbiere"
        },
        {
            "ulx": "2621", "uly": "4599",
            "lrx": "2747", "lry": "4643",
            "corresp": "#Cornet_Chateau"
        },
        {
            "ulx": "6536", "uly": "7036",
            "lrx": "6782", "lry": "7244",
            "corresp": "#Coutances"
        },
        {
            "ulx": "3400", "uly": "7932",
            "lrx": "3572", "lry": "7986",
            "corresp": "#Deree"
        },
        {
            "ulx": "2821", "uly": "5380",
            "lrx": "3526", "lry": "5513",
            "corresp": "#Deroute"
        },
        {
            "ulx": "2734", "uly": "4175",
            "lrx": "2854", "lry": "4216",
            "corresp": "#Deux_Brayes"
        },
        {
            "ulx": "4288", "uly": "5795",
            "lrx": "4475", "lry": "5880",
            "corresp": "#Dirouilles"
        },
        {
            "ulx": "1127", "uly": "6989",
            "lrx": "1577", "lry": "7191",
            "corresp": "#Douvres"
        },
        {
            "ulx": "4718", "uly": "5988",
            "lrx": "4935", "lry": "6103",
            "corresp": "#Ecrehou"
        },
        {
            "ulx": "2207", "uly": "4734",
            "lrx": "2308", "lry": "4786",
            "corresp": "#Foret"
        },
        {
            "ulx": "3380", "uly": "8988",
            "lrx": "3608", "lry": "9072",
            "corresp": "#Frehel"
        },
        {
            "ulx": "3974", "uly": "7122",
            "lrx": "4486", "lry": "7330",
            "corresp": "#Grelets"
        },
        {
            "ulx": "1781", "uly": "4707",
            "lrx": "1938", "lry": "4820",
            "corresp": "#Hanois"
        },
        {
            "ulx": "2888", "uly": "4424",
            "lrx": "3082", "lry": "4616",
            "corresp": "#Herm"
        },
        {
            "ulx": "2507", "uly": "4533",
            "lrx": "2615", "lry": "4567",
            "corresp": "#Hougue_la_Perre"
        },
        {
            "ulx": "2521", "uly": "4762",
            "lrx": "2608", "lry": "4806",
            "corresp": "#Jerbourg"
        },
        {
            "ulx": "2838", "uly": "4619",
            "lrx": "3000", "lry": "4680",
            "corresp": "#Jet_Hou"
        },
        {
            "ulx": "1927", "uly": "4474",
            "lrx": "2096", "lry": "4532",
            "corresp": "#Li_Hou"
        },
        {
            "ulx": "3972", "uly": "7618",
            "lrx": "4148", "lry": "7678",
            "corresp": "#Maisons"
        },
        {
            "ulx": "4126", "uly": "7672",
            "lrx": "4316", "lry": "7732",
            "corresp": "#Maitre_Ile"
        },
        {
            "ulx": "3952", "uly": "7744",
            "lrx": "4192", "lry": "7826",
            "corresp": "#Minquiers"
        },
        {
            "ulx": "3694", "uly": "8468",
            "lrx": "3952", "lry": "8580",
            "corresp": "#Moines"
        },
        {
            "ulx": "3453", "uly": "3230",
            "lrx": "3588", "lry": "3300",
            "corresp": "#Ortach"
        },
        {
            "ulx": "834", "uly": "8502",
            "lrx": "948", "lry": "8656",
            "corresp": "#Paimpol"
        },
        {
            "ulx": "3710", "uly": "5680",
            "lrx": "4093", "lry": "5805",
            "corresp": "#Pater_Noster"
        },
        {
            "ulx": "2027", "uly": "4665",
            "lrx": "2134", "lry": "4726",
            "corresp": "#Plainmont"
        },
        {
            "ulx": "1478", "uly": "9358",
            "lrx": "1712", "lry": "9434",
            "corresp": "#Portrieux"
        },
        {
            "ulx": "2491", "uly": "4198",
            "lrx": "2625", "lry": "4261",
            "corresp": "#Roque_Nord"
        },
        {
            "ulx": "3338", "uly": "4248",
            "lrx": "3580", "lry": "4480",
            "corresp": "#Ruau_Grand"
        },
        {
            "ulx": "2988", "uly": "3953",
            "lrx": "3135", "lry": "4138",
            "corresp": "#Ruau_Petit"
        },
        {
            "ulx": "2603", "uly": "4231",
            "lrx": "2698", "lry": "4300",
            "corresp": "#Sablonneuse"
        },
        {
            "ulx": "2191", "uly": "4589",
            "lrx": "2316", "lry": "4651",
            "corresp": "#Saint_Pierre_du_Bois"
        },
        {
            "ulx": "2381", "uly": "4607",
            "lrx": "2612", "lry": "4647",
            "corresp": "#Saint_Pierre_Port"
        },
        {
            "ulx": "2481", "uly": "4435",
            "lrx": "2624", "lry": "4479",
            "corresp": "#Saint_Sampson"
        },
        {
            "ulx": "2190", "uly": "4526",
            "lrx": "2302", "lry": "4584",
            "corresp": "#Saint_Sauveur"
        },
        {
            "ulx": "1604", "uly": "9952",
            "lrx": "1922", "lry": "10084",
            "corresp": "#Saint-Brieuc"
        },
        {
            "ulx": "2090", "uly": "4320",
            "lrx": "2242", "lry": "4375",
            "corresp": "#Sambule"
        },
        {
            "ulx": "2276", "uly": "4294",
            "lrx": "2398", "lry": "4331",
            "corresp": "#Sauts_Roquiers"
        },
        {
            "ulx": "4560", "uly": "8040",
            "lrx": "4775", "lry": "8163",
            "corresp": "#Sauvages"
        },
        {
            "ulx": "3190", "uly": "4838",
            "lrx": "3448", "lry": "4968",
            "corresp": "#Serk"
        },
        {
            "ulx": "2247", "uly": "4806",
            "lrx": "2366", "lry": "4839",
            "corresp": "#Sommeilleuse"
        },
        {
            "ulx": "4286", "uly": "8170",
            "lrx": "4534", "lry": "8278",
            "corresp": "#Chouas"
        },
        {
            "ulx": "1941", "uly": "4749",
            "lrx": "2078", "lry": "4791",
            "corresp": "#Tas_Pois_Aval"
        },
        {
            "ulx": "2614", "uly": "4370",
            "lrx": "2689", "lry": "4406",
            "corresp": "#Valle"
        },
        {
            "ulx": "2619", "uly": "4396",
            "lrx": "2718", "lry": "4455",
            "corresp": "#Valle_Chateau"
        },
        {
            "ulx": "2200", "uly": "4417",
            "lrx": "2288", "lry": "4494",
            "corresp": "#Vason_Baie"
        }
    ],
    "guernesey": [
        {
            "ulx": "6340", "uly": "1166",
            "lrx": "6740", "lry": "1393",
            "corresp": "#Ancresse"
        },
        {
            "ulx": "7432", "uly": "2157",
            "lrx": "7639", "lry": "2280",
            "corresp": "#Bourdeaux"
        },
        {
            "ulx": "5696", "uly": "2278",
            "lrx": "6490", "lry": "2528",
            "corresp": "#Braye_du_Valle"
        },
        {
            "ulx": "7489", "uly": "1818",
            "lrx": "7645", "lry": "2008",
            "corresp": "#Bu_de_la_rue"
        },
        {
            "ulx": "4815", "uly": "4280",
            "lrx": "5041", "lry": "4374",
            "corresp": "#Catel"
        },
        {
            "ulx": "1993", "uly": "4628",
            "lrx": "2156", "lry": "4719",
            "corresp": "#Catiau_Roque"
        },
        {
            "ulx": "6145", "uly": "3377",
            "lrx": "6341", "lry": "3476",
            "corresp": "#Lierre_Chateau"
        },
        {
            "ulx": "6252", "uly": "1673",
            "lrx": "6806", "lry": "1746",
            "corresp": "#Clotures_Route"
        },
        {
            "ulx": "3651", "uly": "3207",
            "lrx": "4005", "lry": "3420",
            "corresp": "#Cobo_Bay"
        },
        {
            "ulx": "6784", "uly": "4477",
            "lrx": "7217", "lry": "4642",
            "corresp": "#Cornet_Chateau"
        },
        {
            "ulx": "7273", "uly": "2740",
            "lrx": "7526", "lry": "2833",
            "corresp": "#Crevel_Mont_Est"
        },
        {
            "ulx": "2640", "uly": "3990",
            "lrx": "2887", "lry": "4130",
            "corresp": "#Crocq-Point"
        },
        {
            "ulx": "5176", "uly": "4880",
            "lrx": "5417", "lry": "4996",
            "corresp": "#Croix_Baillif"
        },
        {
            "ulx": "5374", "uly": "6861",
            "lrx": "5771", "lry": "7112",
            "corresp": "#Dicart_Cap"
        },
        {
            "ulx": "6639", "uly": "6172",
            "lrx": "6801", "lry": "6322",
            "corresp": "#Doyle_Colonne"
        },
        {
            "ulx": "7398", "uly": "1101",
            "lrx": "7675", "lry": "1197",
            "corresp": "#Doyle_Point"
        },
        {
            "ulx": "5967", "uly": "4306",
            "lrx": "6085", "lry": "4505",
            "corresp": "#Doyle_Road"
        },
        {
            "ulx": "6524", "uly": "5674",
            "lrx": "6871", "lry": "5838",
            "corresp": "#Fermain_Bay"
        },
        {
            "ulx": "6297", "uly": "5051",
            "lrx": "6583", "lry": "5291",
            "corresp": "#George_Fort"
        },
        {
            "ulx": "4312", "uly": "2789",
            "lrx": "4750", "lry": "2909",
            "corresp": "#Grand_Mielles"
        },
        {
            "ulx": "6305", "uly": "4786",
            "lrx": "6492", "lry": "4866",
            "corresp": "#Havelet"
        },
        {
            "ulx": "6485", "uly": "3513",
            "lrx": "6891", "lry": "3657",
            "corresp": "#Hougue_la_Perre"
        },
        {
            "ulx": "4575", "uly": "3012",
            "lrx": "4823", "lry": "3173",
            "corresp": "#Hougue_des_Pommiers"
        },
        {
            "ulx": "7574", "uly": "1696",
            "lrx": "7788", "lry": "1953",
            "corresp": "#Houmet_Paradis"
        },
        {
            "ulx": "7581", "uly": "2081",
            "lrx": "7784", "lry": "2268",
            "corresp": "#Houmet_Benet"
        },
        {
            "ulx": "5178", "uly": "5277",
            "lrx": "5414", "lry": "5375",
            "corresp": "#Huriaux"
        },
        {
            "ulx": "5961", "uly": "5435",
            "lrx": "6202", "lry": "5513",
            "corresp": "#Hubies_Route"
        },
        {
            "ulx": "6515", "uly": "6393",
            "lrx": "6894", "lry": "6636",
            "corresp": "#Jerbourg"
        },
        {
            "ulx": "1322", "uly": "4509",
            "lrx": "1734", "lry": "4786",
            "corresp": "#Leree_Point"
        },
        {
            "ulx": "1130", "uly": "4642",
            "lrx": "1402", "lry": "4857",
            "corresp": "#Li_Hou_Detroit"
        },
        {
            "ulx": "833", "uly": "4441",
            "lrx": "1153", "lry": "4827",
            "corresp": "#Li_Hou"
        },
        {
            "ulx": "702", "uly": "4331",
            "lrx": "911", "lry": "4652",
            "corresp": "#Li_Houmet"
        },
        {
            "ulx": "927", "uly": "6386",
            "lrx": "1212", "lry": "6578",
            "corresp": "#Maison_visionnee"
        },
        {
            "ulx": "5380", "uly": "2303",
            "lrx": "5671", "lry": "2504",
            "corresp": "#Mare_Pelee"
        },
        {
            "ulx": "4297", "uly": "7024",
            "lrx": "4555", "lry": "7125",
            "corresp": "#Moie"
        },
        {
            "ulx": "5565", "uly": "4202",
            "lrx": "5759", "lry": "4248",
            "corresp": "#Mon_Plaisir"
        },
        {
            "ulx": "7536", "uly": "1334",
            "lrx": "7792", "lry": "1526",
            "corresp": "#Omptolle"
        },
        {
            "ulx": "2024", "uly": "4150",
            "lrx": "2411", "lry": "4559",
            "corresp": "#Perrelle_Bay"
        },
        {
            "ulx": "4622", "uly": "6601",
            "lrx": "4905", "lry": "6775",
            "corresp": "#Petit_Bo"
        },
        {
            "ulx": "826", "uly": "6059",
            "lrx": "1174", "lry": "6259",
            "corresp": "#Pezeris"
        },
        {
            "ulx": "709", "uly": "6294",
            "lrx": "1193", "lry": "6582",
            "corresp": "#Plainmont"
        },
        {
            "ulx": "4345", "uly": "2295",
            "lrx": "4594", "lry": "2424",
            "corresp": "#Port_Enfer"
        },
        {
            "ulx": "4982", "uly": "1927",
            "lrx": "5201", "lry": "2034",
            "corresp": "#Port_Grat"
        },
        {
            "ulx": "4293", "uly": "2531",
            "lrx": "4447", "lry": "2654",
            "corresp": "#Port_Soif"
        },
        {
            "ulx": "924", "uly": "5163",
            "lrx": "1709", "lry": "5779",
            "corresp": "#Rocquaine"
        },
        {
            "ulx": "1351", "uly": "5851",
            "lrx": "1662", "lry": "5980",
            "corresp": "#Rocquaine_Chateau"
        },
        {
            "ulx": "3296", "uly": "4535",
            "lrx": "3550", "lry": "4648",
            "corresp": "#Roque_qui_chante"
        },
        {
            "ulx": "5476", "uly": "1842",
            "lrx": "5655", "lry": "1931",
            "corresp": "#Rousse"
        },
        {
            "ulx": "6880", "uly": "2552",
            "lrx": "7189", "lry": "0",
            "corresp": "#Saint_Sampson"
        },
        {
            "ulx": "5670", "uly": "6583",
            "lrx": "6249", "lry": "6875",
            "corresp": "#Saints_Baie"
        },
        {
            "ulx": "5068", "uly": "5836",
            "lrx": "6244", "lry": "6189",
            "corresp": "#Saint_Martin"
        },
        {
            "ulx": "1919", "uly": "5405",
            "lrx": "2793", "lry": "6219",
            "corresp": "#Saint_Pierre_du_Bois"
        },
        {
            "ulx": "2732", "uly": "5969",
            "lrx": "2978", "lry": "6092",
            "corresp": "#Saint_Pierre_du_Bois_Clocher"
        },
        {
            "ulx": "5782", "uly": "3977",
            "lrx": "6475", "lry": "4731",
            "corresp": "#Saint_Pierre_Port"
        },
        {
            "ulx": "6145", "uly": "4269",
            "lrx": "6249", "lry": "4344",
            "corresp": "#SPP_Cimetiere"
        },
        {
            "ulx": "6123", "uly": "4163",
            "lrx": "6330", "lry": "4188",
            "corresp": "#Hyvreuse"
        },
        {
            "ulx": "6279", "uly": "4646",
            "lrx": "6428", "lry": "4866",
            "corresp": "#SPP_Hauteville"
        },
        {
            "ulx": "6504", "uly": "4215",
            "lrx": "6863", "lry": "4525",
            "corresp": "#SPP_Port"
        },
        {
            "ulx": "6323", "uly": "4492",
            "lrx": "6434", "lry": "4532",
            "corresp": "#SPP_Eglise"
        },
        {
            "ulx": "6508", "uly": "3993",
            "lrx": "6756", "lry": "4095",
            "corresp": "#SPP_Salerie"
        },
        {
            "ulx": "6072", "uly": "4332",
            "lrx": "6220", "lry": "4398",
            "corresp": "#Tour_Victoria"
        },
        {
            "ulx": "7027", "uly": "2774",
            "lrx": "7123", "lry": "2839",
            "corresp": "#Saint_Sampson_Eglise"
        },
        {
            "ulx": "6925", "uly": "2602",
            "lrx": "7203", "lry": "2764",
            "corresp": "#Saint_Sampson_Port"
        },
        {
            "ulx": "4251", "uly": "6617",
            "lrx": "4573", "lry": "6829",
            "corresp": "#Sommeilleuse"
        },
        {
            "ulx": "6485", "uly": "6744",
            "lrx": "6746", "lry": "6849",
            "corresp": "#Tau_Pez_Amont"
        },
        {
            "ulx": "1152", "uly": "6947",
            "lrx": "1345", "lry": "7058",
            "corresp": "#Tas_Pois_Aval"
        },
        {
            "ulx": "2168", "uly": "6809",
            "lrx": "2413", "lry": "7038",
            "corresp": "#Tielles"
        },
        {
            "ulx": "1043", "uly": "6356",
            "lrx": "1599", "lry": "6839",
            "corresp": "#Torteval"
        },
        {
            "ulx": "5937", "uly": "1769",
            "lrx": "7036", "lry": "0",
            "corresp": "#Valle"
        },
        {
            "ulx": "7139", "uly": "2442",
            "lrx": "7380", "lry": "0",
            "corresp": "#Valle_Chateau"
        },
        {
            "ulx": "2803", "uly": "3731",
            "lrx": "3332", "lry": "4085",
            "corresp": "#Vason_Baie"
        },
        {
            "ulx": "3863", "uly": "4818",
            "lrx": "4142", "lry": "4954",
            "corresp": "#Videclins_Route"
        }
    ]
}




var data = {
    "#Guernesey": 100,
    "#Archipel": 15,
    "#Saint_Pierre_Port": 32,
    "#Valle": 5,
    "#Saint_Sampson": 61,
    "#Eldad_Chapelle": 1,
    "#Irlande": 3,
    "#Basses_Maisons": 2,
    "#Lierre_Chateau": 1,
    "#Bu_de_la_rue": 36,
    "#Maison_visionnee": 11,
    "#Jersey": 27,
    "#Manche": 14,
    "#France": 32,
    "#Espagne": 4,
    "#Angleterre": 34,
    "#Ortach": 4,
    "#Aurigny": 10,
    "#Casquets": 5,
    "#Houmet_Paradis": 8,
    "#Saint_Sampson_Port": 4,
    "#Valle_Chateau": 5,
    "#Bordage_Carrefour": 3,
    "#Roque_Crespel": 2,
    "#Rocquaine": 5,
    "#Dunkerque": 4,
    "#Ancresse_Dolmen": 1,
    "#Roque_qui_chante": 3,
    "#Torteval": 19,
    "#Grand_Mielles": 1,
    "#Sommeilleuse": 1,
    "#Catiau_Roque": 1,
    "#Godaines": 1,
    "#Fontenelle_Demie": 1,
    "#Arculons": 1,
    "#Huriaux": 1,
    "#Hamel": 1,
    "#Epine": 1,
    "#Normandie": 2,
    "#Clos-Landes": 1,
    "#Bretagne": 4,
    "#B_01": 1,
    "#B_02": 1,
    "#B_03": 1,
    "#B_04": 1,
    "#B_05": 1,
    "#B_06": 1,
    "#B_07": 1,
    "#B_08": 1,
    "#B_09": 1,
    "#B_10": 1,
    "#B_11": 1,
    "#B_12": 1,
    "#Herm": 10,
    "#Hollande": 2,
    "#Serk": 14,
    "#Brecqhou": 3,
    "#Chousey": 3,
    "#Chaise_Gild_Holm_Ur": 14,
    "#Londres": 7,
    "#Corne_Bete": 1,
    "#Pinacle": 2,
    "#Chaise_Moine": 1,
    "#Weymouth": 1,
    "#Courseulle": 1,
    "#Rochefort": 3,
    "#Granville": 4,
    "#Saint_Malo": 39,
    "#Portbail": 1,
    "#Charente": 2,
    "#Paris": 15,
    "#Wales": 1,
    "#Europe": 7,
    "#Dinan": 1,
    "#Saint-Brieuc": 1,
    "#Rennes": 1,
    "#Saint_Sampson_Bravees": 35,
    "#Colette": 1,
    "#Ermitage": 1,
    "#Elisabeth_Chateau": 1,
    "#Hanois": 20,
    "#Plainmont": 25,
    "#Minquiers": 11,
    "#Ancresse": 3,
    "#Winchester": 2,
    "#Wight": 1,
    "#Cornet_Chateau": 1,
    "#Auberge_Jean": 12,
    "#Binic": 2,
    "#Bilbao": 3,
    "#Greenwich": 1,
    "#Belgique": 1,
    "#Dieppe": 1,
    "#Lorient": 1,
    "#Oyarzun": 1,
    "#Hanois_Grand": 5,
    "#Hanois_Petit": 3,
    "#Hanois_Mauve": 4,
    "#Hanois_Light_Red": 1,
    "#Hanois_13_Rochers": 1,
    "#Hautes_Fourquies": 1,
    "#Aiguillons": 1,
    "#Herouee": 1,
    "#Cat_Rock": 1,
    "#Percee": 1,
    "#Roque_Herpin": 1,
    "#Boue_South": 1,
    "#Boue_le_Mouet": 1,
    "#Roque_Ronde": 2,
    "#Roque_Rouge": 1,
    "#Portland": 4,
    "#Tor Bay": 7,
    "#Decolle_Pointe": 2,
    "#Banquetiers": 1,
    "#Douvres": 203,
    "#Brehant_Cap": 1,
    "#Corbiere": 2,
    "#Hagot": 1,
    "#Brehat": 1,
    "#Cancale": 1,
    "#Cesambre": 1,
    "#Ecrehou": 1,
    "#Calvados_Banc": 1,
    "#Aiguilles_Wight": 1,
    "#Ronesse": 1,
    "#Beaulieu_Cote": 1,
    "#Merquel": 1,
    "#Etables": 1,
    "#Plouha": 1,
    "#Anderlo_Vieux": 1,
    "#Anderlo_Petit": 2,
    "#Ras_Ile": 1,
    "#Mortes_Femmes": 1,
    "#Boue_Passage": 1,
    "#Frouquie_Passage": 1,
    "#Deroute": 1,
    "#Hardent": 1,
    "#Mauvais_Cheval": 1,
    "#Boulay_Bay": 1,
    "#Barneville": 1,
    "#Pater_Noster": 2,
    "#Dicart_Cap": 1,
    "#Gros_Nez": 1,
    "#GB": 1,
    "#Carteret": 2,
    "#Coutances": 1,
    "#Bordage": 1,
    "#Digoville": 1,
    "#Ostende": 1,
    "#Exeter": 1,
    "#Thouars": 2,
    "#La_Rochelle": 1,
    "#Grelets": 2,
    "#Chouas": 6,
    "#Sauvages": 2,
    "#Moines": 2,
    "#Canard": 4,
    "#Deree": 1,
    "#Maitre_Ile": 1,
    "#Maisons": 2,
    "#Azette_Greve": 1,
    "#Dirouilles": 2,
    "#Pasages": 1,
    "#Hougue_la_Perre": 1,
    "#Bourdeaux": 1,
    "#Houmet_Benet": 1,
    "#Platon": 1,
    "#Port_Grat": 1,
    "#Vason_Baie": 2,
    "#Perrelle_Bay": 1,
    "#Pezeris": 1,
    "#Tielles": 1,
    "#Petit_Bo": 1,
    "#Omptolle": 3,
    "#Deux_Brayes": 1,
    "#Grunes": 1,
    "#B_13": 1,
    "#B_14": 2,
    "#Fontenelle_Pointe": 1,
    "#Crevel_Mont_Ouest": 1,
    "#Clotures_Route": 1,
    "#Sablonneuse": 1,
    "#Mare_Pelee": 1,
    "#Quenon": 1,
    "#Rousse_Mer": 1,
    "#Gripe_Rousse": 1,
    "#Port_Soif": 1,
    "#Port_Enfer": 1,
    "#Boue_Corneille": 1,
    "#Moulrette": 1,
    "#Catel": 1,
    "#Cobo_Bay": 2,
    "#Boue_Jardin": 1,
    "#Grunettes": 1,
    "#Suzanne": 1,
    "#Grunes_Ouest": 1,
    "#Messellettes": 1,
    "#Cotillon_Pipet": 1,
    "#Jennerotte": 1,
    "#Crocq-Point": 1,
    "#Equerrier": 1,
    "#Colombelle": 1,
    "#Souffleresse": 2,
    "#Li_Hou": 4,
    "#Tau_Pez_Amont": 1,
    "#Tas_Pois_Aval": 1,
    "#Silleuse": 1,
    "#Marquis_Banc": 1,
    "#Grand_Etacre": 1,
    "#Li_Houmet": 1,
    "#Pecheresse": 1,
    "#Angullieres": 1,
    "#Leree_Point": 1,
    "#Saint_Pierre_du_Bois_Clocher": 1,
    "#Rocquaine_Chateau": 1,
    "#Haute_Canee": 1,
    "#Sambule": 1,
    "#Moie": 3,
    "#Boue_Blondel": 1,
    "#Gosselin_Havre": 1,
    "#Aiguilles_Banc": 1,
    "#Roque_au_Diable": 1,
    "#Pyrenees": 1,
    "#Plemont_Caves": 1,
    "#Creux_Maille": 1,
    "#Boutiques_Serk": 1,
    "#Mont_Saint_Michel": 1,
    "#Boisrose_Falaise": 1,
    "#Ambleteuse_Baie": 1,
    "#Grand_V": 1,
    "#Isigny": 1,
    "#Anderlo_Grand": 2,
    "#Saint_Sampson_Cloche_Port": 1,
    "#Southampton": 1,
    "#Havelet": 9,
    "#Hyvreuse": 1,
    "#SPP_Hauteville": 1,
    "#George_Fort": 1,
    "#Banques_Pointe": 1,
    "#SPP_Salerie": 1
};