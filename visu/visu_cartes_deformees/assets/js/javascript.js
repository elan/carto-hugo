document.addEventListener("DOMContentLoaded",  function() {
    init();
}, false);

function init() {

    // click sur les tabs pour montrer/cacher une cible
    var tabs = document.querySelectorAll(".tab_container .tab_item");
    tabs.forEach(tab => tab.addEventListener("click", handleTabClick));

    // click pr toggle
    var el = document.querySelector("#toggle_map");
    el.addEventListener("click", toggle_map);

    appendZones("europe", data);
    appendZones("archipel", data);
    appendZones("guernesey", data);

    // resetZones("europe");

}

function toggle_map() {
    var tab_target = document.querySelectorAll(".tab_target");
    tab_target.forEach(el => el.classList.toggle("img_carte"));
}

function resetZones(target) {
    var svg = document.querySelector("[data-target='" + target + "'] svg");
    var circles = svg.querySelectorAll("circle");
    for (var i = 0; i < circles.length; i++) {
        svg.removeChild(circles[i])
    }
}

function appendZones(target, data) {
    var svg = document.querySelector("[data-target='" + target + "'] svg");

    // calcul du max
    var zones_with_occ = zones[target].filter(place => data[place.corresp] != undefined);
    var occ_zones = zones_with_occ.map(place => data[place.corresp]);
    var occ_max = Math.max(...occ_zones);

    for (var i = 0; i < zones[target].length; i++) {
        var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

        if (target == "guernesey") {
            var ratio = 4.1;
        } else {
            var ratio = 1;
        }

        circle.setAttribute('cx', zones[target][i].ulx / ratio);
        circle.setAttribute('cy', zones[target][i].uly / ratio);

        var occ = data[zones[target][i].corresp];

        // si on a de la donnée pr ce corresp
        if (occ != undefined) {

            var num = scale(occ, 1, occ_max, 20, 150);

            circle.setAttribute('r', num / ratio);
            circle.setAttribute('fill', "aquamarine");
            circle.setAttribute('stroke', "black");
            circle.setAttribute('stroke-width', 10 / ratio);

            var title = document.createElementNS('http://www.w3.org/2000/svg', 'title');
            title.textContent = zones[target][i].corresp + " " + occ;
            circle.appendChild(title);

            svg.appendChild(circle);
        } else {

        }

    }

}


function scale (number, inMin, inMax, outMin, outMax) {
    /*
        interval de départ : outMin, outMax
        interval à atteindre : outMin, outMax
    */
    return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}


function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min +1)) + min;
}

